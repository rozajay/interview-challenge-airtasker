import * as React from 'react'
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
// import { withKnobs } from '@storybook/addon-knobs';
// import { Footer } from 'src/components/Footer/Footer.ui';

import { Button, Welcome } from '@storybook/react/demo';
storiesOf('Components|Footer', module)
.add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
