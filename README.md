# Airtaskter Front End Test

The objective is to create an Airtasker activity feed application. 

## How the project progressed

Wow, that was a super fun front end task. Initially upon receiving the task I had some experience using React only so Version 1 of the project was done in React only. Version 1 progressed fairly quickyly but haphazardly as I wrote the project component by component then integrated things as I went along. Little planning was involved as I was not too sure how to process everything and integrate it all together. Another two tricky aspect for me was how to write tests and then what does it mean for code to be production ready.

Fast forward to March, I moved to a new team that uses Typescript and I picked it up fairly fast. I quite liked the Typescript's strong typing so decided to rewrite the project using React integrated with Typscript and Redux. I suppose the use of Redux is a bit overkill for this project but I had wanted to learn how to do Redux properly with React for ages so decided to use this as a project to learn it. 

Thanks ahead for reviewing my code, there was a few things that I struggled with along the way (I will list current code issues below) but overall it was fun and I have learnt alot :). 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing 

Unzip the folder open.

Install node modules
```
cd activity-feed
npm install
```

Start the server
```
npm run startAPI
```

Open a new terminal 
```
npm start
```

If the window doesn't automatically pop up, open the url 'http://localhost:3000/' to access the application locally

## Running the tests

```
npm run testJest
```

## Areas of improvement

- Still need test coverage for async functions
- Update API call to account for async update should the api content change
- Improve quality of tests in general
- Overall styling can be improved
- Proper configuration of storybook
- React Testing Library
- Snapshot Testing

## Deployment

Use below command to generate the latest version of production build

```
npm run build
```

Use Static server to serve static site with the below command.

```
serve -s build -l 4000
```

## Author

* **Roza Jiang** 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
