import * as React from 'react';
import { mount } from 'enzyme';
import 'jest';

import { ActivityItem } from '../../../../src/components/ActivityItem/ActivityItem.ui'

describe('renders ActivityItem', () => {
  let props;
  let mountedActivityItem;

  const activityItem = () => {
    if (!mountedActivityItem) {
      mountedActivityItem = mount(<ActivityItem {...props} />)
    }
    return mountedActivityItem;

  }

  beforeEach(() => {
    props = {
      data: { template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] },
      profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
      tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
    };
    mountedActivityItem = undefined;
  });

  test('should render the ActivityItem', function () {
    activityItem()
    expect(mountedActivityItem).toBeDefined()
  })
})