import * as React from 'react';
import { shallow } from 'enzyme';
import 'jest';

import { ActivityList } from '../../../../src/components/ActivityList/ActivityList.ui'

describe('renders ActivityList', () => {
  let props
  let mountedActivityList;

  const activityList = () => {
    if (!mountedActivityList) {
      mountedActivityList = shallow(<ActivityList {...props} />)
    }
    return mountedActivityList;

  }


  beforeEach(() => {
    props = {
      activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
    };
    mountedActivityList = undefined;
  });

  test('should render the ActivityList', function () {
    activityList()
    expect(mountedActivityList).toBeDefined()
  })
})