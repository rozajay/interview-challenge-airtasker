// import * as React from 'react';
// import { mount } from 'enzyme';
import 'jest';

// import { App } from '../../src/components/App/App.ui'

describe('renders App', () => {
  // let props
  let mountedApp;

  const app = () => {
    if (!mountedApp) {
      mountedApp = 'mount(<App/>)'
    }
    return mountedApp;

  }
  test('should render the App', function () {
    app()
    expect(mountedApp).toBeDefined()
  })
})