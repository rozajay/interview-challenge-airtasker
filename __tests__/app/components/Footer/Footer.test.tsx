import * as React from 'react';
import { mount } from 'enzyme';
import 'jest';

import { Footer } from '../../../../src/components/Footer/Footer.ui'

describe('renders Footer', () => {
  const props = { slugPath: 'hai' }
  let mountedFooter;

  const footer = () => {
    if (!mountedFooter) {
      mountedFooter = mount(<Footer {...props} />)
    }
    return mountedFooter;

  }
  test('should render the Footer', function () {
    footer()
    expect(mountedFooter).toBeDefined()
  })
})