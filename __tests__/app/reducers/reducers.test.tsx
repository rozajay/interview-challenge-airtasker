import { reducer } from '../../../src/reducers/index'
import * as types from '../../../src/constants/index'

describe('reducer', () => {

  test('should handle NO DISPLAY SLUG', () => {
    expect(
      reducer({
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }, {
          type: types.NO_DISPLAY_SLUG,
        })
    ).toEqual(
      {
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }
    )
  })

  test('should handle CHANGE_SLUG', () => {
    expect(
      reducer({
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }, {
          type: types.CHANGE_SLUG,
          slugPath: '/new/path'
        })
    ).toEqual(
      {
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: '/new/path'
      }
    )
  })

  test('should handle RECEIVE ACTIVITY FEED', () => {
    expect(
      reducer({
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }, {
          type: types.RECEIVE_ACTIVITY_FEED,
          activityFeed: [{ template: "{ Changed Profile } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
          receivedAt: Date.now()
        })
    ).toEqual(
      {
        activityFeed: [{ template: "{ Changed Profile } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }
    )
  })

  test('should handle RECEIVE PROFILES', () => {
    expect(
      reducer({
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }, {
          type: types.RECEIVE_PROFILES,
          profiles: [{ abbreviated_name: "New Profile", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
          receivedAt: Date.now()
        })
    ).toEqual(
      {
        activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
        profiles: [{ abbreviated_name: "New Profile", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
        tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
        slugPath: 'Mouse over a task or a name to get their path'
      }
    )
  })
})

test('should handle RECEIVE TASKS', () => {
  expect(
    reducer({
      activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
      profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
      tasks: [{ name: "Buy me McDonalds", id: 6333, slug: "f.b.f" }],
      slugPath: 'Mouse over a task or a name to get their path'
    }, {
        type: types.RECEIVE_TASKS,
        tasks: [{ name: "new task", id: 6333, slug: "f.b.f" }],
        receivedAt: Date.now()
      })
  ).toEqual(
    {
      activityFeed: [{ template: "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }", task_id: 6333, profile_ids: [37, 1501] }],
      profiles: [{ abbreviated_name: "Simon R.", id: 37, slug: "simon-rodwell" }, { abbreviated_name: "Jonathan L.", id: 1501, slug: "jonathan-lui" }],
      tasks: [{ name: "new task", id: 6333, slug: "f.b.f" }],
      slugPath: 'Mouse over a task or a name to get their path'
    }
  )
})
