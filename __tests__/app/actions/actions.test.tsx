import * as actions from '../../../src/actions/index'
import * as types from '../../../src/constants/index'
// import thunk from 'redux-thunk'
// import * as fetchMock from 'fetch-mock'
// import configureMockStore from 'redux-mock-store'
// const middlewares = [thunk]
// const mockStore = configureMockStore(middlewares)

describe('actions', () => {

  // Test for noDisplaySlug
  test('should create an action to not display slug', () => {
    const expectedAction = {
      type: types.NO_DISPLAY_SLUG
    }
    expect(actions.noDisplaySlug()).toEqual(expectedAction)
  })

  // Test for changeSlug
  test('should create an action to change slug', () => {
    const slugPath = 'string'
    const expectedAction = {
      type: types.CHANGE_SLUG,
      slugPath
    }
    expect(actions.changeSlug(slugPath)).toEqual(expectedAction)
  })

  // Test for receiveActivityFeed
  test('should create an action to receive Activity Feed', () => {
    const json = [{
      "created_at": "2015-06-24T16:34:00+10:00",
      "template": "{ profiles:2046 } posted a task { task:6473 }",
      "event": "posted",
      "task_id": 6473,
      "profile_ids": [
        2046
      ]
    }]
    const activityFeed = [{
      "template": "{ profiles:2046 } posted a task { task:6473 }",
      "task_id": 6473,
      "profile_ids": [
        2046
      ]
    }]
    const receivedAt = Date.now()
    const expectedAction = {
      type: types.RECEIVE_ACTIVITY_FEED,
      activityFeed,
      receivedAt
    }
    expect(actions.receiveActivityFeed(json)).toEqual(expectedAction)
  })

  // Test for receiveTasks
  test('should create an action to receive Tasks', () => {
    const json = [{
      "default_location_id": 1830,
      "runners_required_count": 1,
      "runners_assigned_count": 0,
      "project": false,
      "origin_task_id": 0,
      "origin_task_slug": null,
      "clone_task_ids": [],
      "clone_task_slugs": [],
      "id": 6441,
      "name": "Teach me how to fly a drone",
      "slug": "drone",
      "bids_count": 0,
      "price": 70,
      "assigned_price": null,
      "comments_count": 0,
      "deadline": "2015-07-01T23:59:59+10:00",
      "online_or_phone": false,
      "fixed_price": false,
      "state": "posted",
      "created_at": "2015-06-23T17:06:50+10:00",
      "first_posted_at": "2015-06-24T16:36:15+10:00",
      "posted_or_edited_at": "2015-06-24T16:36:15+10:00",
      "private_messages_count": 0,
      "sender_id": 490,
      "runner_id": null,
      "location_ids": [
        1830
      ],
      "sort_present": false,
      "distance": null
    },]
    const tasks = [{
      "id": 6441,
      "name": "Teach me how to fly a drone",
      "slug": "drone"
    },]
    const receivedAt = Date.now()
    const expectedAction = {
      type: types.RECEIVE_TASKS,
      tasks,
      receivedAt
    }
    expect(actions.receiveTasks(json)).toEqual(expectedAction)
  })

  // Test for receiveProfiles
  test('should create an action to receive Profiles', () => {
    const json = [{
      "average_rating": 3.9,
      "comments_count": 173,
      "allow_calls_on_tasks": true,
      "default_location_id": 1830,
      "id": 490,
      "first_name": "James",
      "avatar": {
        "url": "https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png",
        "source_url": "https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png",
        "resize_url": "https://eu7cmie.cloudimg.io/s/crop/_WIDTH_x_HEIGHT_/_SOURCE_",
        "tiny": {
          "url": "https://eu7cmie.cloudimg.io/s/crop/21x21/https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png"
        },
        "small": {
          "url": "https://eu7cmie.cloudimg.io/s/crop/32x32/https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png"
        },
        "thumb": {
          "url": "https://eu7cmie.cloudimg.io/s/crop/50x50/https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png"
        },
        "medium": {
          "url": "https://eu7cmie.cloudimg.io/s/crop/75x75/https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png"
        },
        "profile": {
          "url": "https://eu7cmie.cloudimg.io/s/crop/128x128/https://dev-assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/490/Screen_Shot_2014-12-10_at_2-63839b7c113316073c1e85a847415e06.59.23_pm.png"
        }
      },
      "abbreviated_name": "James T.",
      "slug": "james-tippett",
      "pro": false,
      "posted_tasks_count": 240,
      "run_tasks_count": 87,
      "received_reviews_count": 46,
      "ranking_position": null,
      "ranking": 59805
    }]
    const profiles = [{
      "id": 490,
      "abbreviated_name": "James T.",
      "slug": "james-tippett",
    },]
    const receivedAt = Date.now()
    const expectedAction = {
      type: types.RECEIVE_PROFILES,
      profiles,
      receivedAt
    }
    expect(actions.receiveProfiles(json)).toEqual(expectedAction)
  })
})

// describe('async actions', () => {
//   afterEach(() => {
//     fetchMock.restore()
//   })

//   it('create ', async () => {
//     fetchMock.getOnce('/activityFeed', {
//       body: {
//         activityFeed: [{
//           "template": "{ profiles:2046 } posted a task { task:6473 }",
//           "task_id": 6473,
//           "profile_ids": [
//             2046
//           ]
//         }]
//       },
//       headers: { 'content-type': 'application/json' }
//     })
//   })
//   const expectedActions = [
//     {
//       type: types.RECEIVE_ACTIVITY_FEED, activityFeed: [{
//         "template": "{ profiles:2046 } posted a task { task:6473 }",
//         "task_id": 6473,
//         "profile_ids": [
//           2046
//         ]
//       }]
//     }
//   ]
//   const store = mockStore({ activityFeed: [] })

//   return store.dispatch(actions.fetchActivityFeed()).then(() => {
//     expect(store.getActions()).toEqual(expectedActions)
//   })
// })