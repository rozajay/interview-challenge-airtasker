import { ApplicationState } from '../state/ApplicationState';

export const InitialState: ApplicationState = {
  activityFeed: [
    {
      "template": "{ profiles:490 } posted a task { task:6441 }",
      "task_id": 6441,
      "profile_ids": [
        490
      ]
    },
    {
      "template": "{ profiles:2046 } posted a task { task:6473 }",
      "task_id": 6473,
      "profile_ids": [
        2046
      ]
    },
    {
      "template": "{ profiles:1501 } completed { task:6333 }",
      "task_id": 6333,
      "profile_ids": [
        1501
      ]
    },
    {
      "template": "{ profiles:37 } assigned { task:6333 } to { profiles:1501 }",
      "task_id": 6333,
      "profile_ids": [
        37,
        1501
      ]
    },
    {
      "template": "{ profiles:1501 } bid on { task:6333 }",
      "task_id": 6333,
      "profile_ids": [
        1501
      ]
    },
    {
      "template": "{ profiles:1501 } commented on { task:6333 }",
      "task_id": 6333,
      "profile_ids": [
        1501
      ]
    },
    {
      "template": "{ profiles:490 } posted a task { task:6472 }",
      "task_id": 6472,
      "profile_ids": [
        490
      ]
    },
    {
      "template": "{ profiles:2046 } posted a task { task:6471 }",
      "task_id": 6471,
      "profile_ids": [
        2046
      ]
    },
    {
      "template": "{ profiles:2663 } posted a task { task:6470 }",
      "task_id": 6470,
      "profile_ids": [
        2663
      ]
    },
    {
      "template": "{ profiles:2663 } signed up",
      "profile_ids": [
        2663
      ]
    }
  ],
  profiles: [{
    "id": 490,
    "abbreviated_name": "James T.",
    "slug": "james-tippett",
  },
  {
    "id": 2046,
    "abbreviated_name": "Kang C.",
    "slug": "kang-chen",
  },
  {

    "id": 37,
    "abbreviated_name": "Simon R.",
    "slug": "simon-rodwell",
  },
  {
    "id": 1501,
    "abbreviated_name": "Jonathan L.",
    "slug": "jonathan-lui",
  },
  {
    "id": 2663,
    "abbreviated_name": "Edward T.",
    "slug": "edward-tippett",
  }],
  tasks: [{
    "id": 6441,
    "name": "Teach me how to fly a drone",
    "slug": "drone",
  },
  {
    "id": 6473,
    "name": "Guitar lessons",
    "slug": "guitar",
  },
  {

    "id": 6333,
    "name": "Buy me McDonalds",
    "slug": "maccas",

  },
  {

    "id": 6472,
    "name": "Pick my car up from garage",
    "slug": "garage",

  },
  {

    "id": 6471,
    "name": "Do a react javascript test for me",
    "slug": "react-javascript",

  },
  {

    "id": 6470,
    "name": "Take me to rails camp",
    "slug": "rails-camp",

  },
  {

    "id": 6469,
    "name": "Pick up roses from florist",
    "slug": "roses",

  }
  ],
  slugPath: 'Mouse over a task or a name to get their path'
}