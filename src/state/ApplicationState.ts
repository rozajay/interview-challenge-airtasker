import { ProfileStruct, DataStruct, TaskStruct } from '../types/index';

export interface ApplicationState {
  activityFeed: DataStruct[],
  profiles: ProfileStruct[],
  tasks: TaskStruct[],
  slugPath: string
}