import { App } from '../components/App/App.ui';
import * as actions from '../actions/index';
import { ApplicationState } from '../state/ApplicationState';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export function mapStateToProps({
  activityFeed,
  profiles,
  tasks }: ApplicationState) {
  return {
    activityFeed,
    profiles,
    tasks
  }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.ApplicationAction>) {
  return {
    fetchActivityFeed: () => dispatch(actions.fetchActivityFeed()),
    fetchProfiles: () => dispatch(actions.fetchProfiles()),
    fetchTasks: () => dispatch(actions.fetchTasks()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);