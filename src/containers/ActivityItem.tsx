import { ActivityItem } from '../components/ActivityItem/ActivityItem.ui';
import * as actions from '../actions/index';
import { ApplicationState } from '../state/ApplicationState';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export function mapStateToProps({ activityFeed,
  profiles,
  tasks }: ApplicationState) {
  return {
    activityFeed,
    profiles,
    tasks
  }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.ApplicationAction>, ownProps: any) {
  return {
    NoDisplaySlug: () => dispatch(actions.noDisplaySlug()),
    ChangeSlug: (slugpath: string) => dispatch(actions.changeSlug(slugpath))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityItem);