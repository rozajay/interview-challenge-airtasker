import { Footer } from '../components/Footer/Footer.ui';
import { ApplicationState } from '../state/ApplicationState';
import { connect } from 'react-redux';

export function mapStateToProps({ slugPath }: ApplicationState) {
  return {
    slugPath
  }
}

export default connect(mapStateToProps)(Footer);