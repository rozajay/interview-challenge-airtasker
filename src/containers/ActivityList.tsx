import { ActivityList } from '../components/ActivityList/ActivityList.ui';
import { ApplicationState } from '../state/ApplicationState';
import { connect } from 'react-redux';

export function mapStateToProps({ activityFeed, profiles, tasks }: ApplicationState) {
  return {
    activityFeed,
    profiles,
    tasks
  }
}

export default connect(mapStateToProps)(ActivityList);