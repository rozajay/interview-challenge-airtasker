import * as constants from '../constants'
import { DataStruct, ProfileStruct, TaskStruct } from 'src/types/index';

export interface NoDisplaySlug {
  type: constants.NO_DISPLAY_SLUG;
}

export interface ChangeSlug {
  type: constants.CHANGE_SLUG;
  slugPath: string
}

export interface ReceiveActivityFeed {
  type: constants.RECEIVE_ACTIVITY_FEED;
  activityFeed: DataStruct[];
  receivedAt: number
}

export interface ReceiveProfiles {
  type: constants.RECEIVE_PROFILES;
  profiles: ProfileStruct[];
  receivedAt: number
}

export interface ReceiveTasks {
  type: constants.RECEIVE_TASKS;
  tasks: TaskStruct[];
  receivedAt: number
}

export type ApplicationAction = NoDisplaySlug | ChangeSlug | ReceiveActivityFeed | ReceiveProfiles | ReceiveTasks


export function noDisplaySlug(): NoDisplaySlug {
  return {
    type: constants.NO_DISPLAY_SLUG
  }
}

export function changeSlug(passedSlugPath: string): ChangeSlug {
  return {
    type: constants.CHANGE_SLUG,
    slugPath: passedSlugPath
  }
}

export function receiveActivityFeed(json: any): ReceiveActivityFeed {

  const relevantInfo = ({ template, task_id, profile_ids }: DataStruct) => {
    return { template, task_id, profile_ids }
  }
  const activityFeedInfo = json.map((output: any) =>
    relevantInfo(output)
  )
  return {
    type: constants.RECEIVE_ACTIVITY_FEED,
    activityFeed: activityFeedInfo,
    receivedAt: Date.now()
  }
}

export function receiveProfiles(json: any): ReceiveProfiles {
  const relevantInfo = ({ abbreviated_name, id, slug }: ProfileStruct) => {
    return { abbreviated_name, id, slug }
  }
  const profilesInfo = json.map((output: any) =>
    relevantInfo(output)
  )
  return {
    type: constants.RECEIVE_PROFILES,
    profiles: profilesInfo,
    receivedAt: Date.now()
  }
}

export function receiveTasks(json: any): ReceiveTasks {
  const relevantInfo = ({ name, id, slug }: TaskStruct) => {
    return { name, id, slug }
  }
  const tasksInfo = json.map((output: any) =>
    relevantInfo(output)
  )
  return {
    type: constants.RECEIVE_TASKS,
    tasks: tasksInfo,
    receivedAt: Date.now()
  }
}


export function fetchActivityFeed(): any {
  return (dispatch: any) => {
    return fetch("http://localhost:3004/activity_feed")
      .then(response => response.json())
      .then(json => dispatch(receiveActivityFeed(json)))
  }
}

export function fetchProfiles(): any {
  return (dispatch: any) => {
    return fetch("http://localhost:3004/profiles")
      .then(response => response.json())
      .then(json => dispatch(receiveProfiles(json)))
  }
}

export function fetchTasks(): any {
  return (dispatch: any) => {
    return fetch("http://localhost:3004/tasks")
      .then(response => response.json())
      .then(json => dispatch(receiveTasks(json)))
  }
}