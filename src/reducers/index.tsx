import { ApplicationAction } from '../actions';
import { NO_DISPLAY_SLUG, RECEIVE_ACTIVITY_FEED, RECEIVE_PROFILES, RECEIVE_TASKS, CHANGE_SLUG } from '../constants/index';
import { ApplicationState } from '../state/ApplicationState'

export function reducer(state: ApplicationState, action: ApplicationAction): ApplicationState {
  switch (action.type) {
    case NO_DISPLAY_SLUG: 
      return { ...state, slugPath: 'Mouse over a task or a name to get their path' };
    case CHANGE_SLUG:
      return { ...state, slugPath: action.slugPath };
    case RECEIVE_ACTIVITY_FEED:
      return { ...state, activityFeed: action.activityFeed };
    case RECEIVE_PROFILES:
      return { ...state, profiles: action.profiles };
    case RECEIVE_TASKS:
      return { ...state, tasks: action.tasks };
  }
  return state;
}


