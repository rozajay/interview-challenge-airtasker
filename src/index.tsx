import * as React from 'react';
import * as ReactDOM from 'react-dom';
import thunkMiddleware from 'redux-thunk'
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux';
import { reducer } from './reducers/index';
import { Provider } from 'react-redux';
import { InitialState } from './state/InitialState'
import { ApplicationState } from './state/ApplicationState'
import './index.css';
import App from './containers/App'

const store = createStore<ApplicationState, any, any, any>(reducer, InitialState, applyMiddleware(thunkMiddleware))

ReactDOM.render(
  <Provider store={store}>
    <App />,
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
