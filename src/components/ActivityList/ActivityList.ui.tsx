import * as React from 'react';
import List from '@material-ui/core/List';
import ActivityItem from 'src/containers/ActivityItem'
import { DataStruct, ProfileStruct, TaskStruct } from 'src/types/index';

export interface Props {
  activityFeed: DataStruct[]
  profiles: ProfileStruct[];
  tasks: TaskStruct[];
}

export class ActivityList extends React.Component<Props> {
  render() {
    const { activityFeed } = this.props
    return (
      <List component="nav">
        {activityFeed.map((data, i) => {
          return <ActivityItem key={i} data={data} />
        })}
      </List>
    )
  }
}