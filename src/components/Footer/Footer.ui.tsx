import * as React from 'react';
import ListItem from '@material-ui/core/ListItem';

export interface Props {
  slugPath: string;
}

export class Footer extends React.Component<Props> {
  render() {
    return (
      <ListItem className="footer-text">{this.props.slugPath}</ListItem>
    )
  }
}