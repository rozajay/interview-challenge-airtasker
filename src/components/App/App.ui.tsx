import * as React from 'react';
import './App.css';
import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import Footer from 'src/containers/Footer'
import ActivityList from 'src/containers/ActivityList'
import { DataStruct, ProfileStruct, TaskStruct } from 'src/types/index';

export interface Props {
  activityFeed: DataStruct[];
  profiles: ProfileStruct[];
  tasks: TaskStruct[];
  fetchActivityFeed: () => void,
  fetchProfiles: () => void,
  fetchTasks: () => void
}

export class App extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchActivityFeed()
    this.props.fetchProfiles();
    this.props.fetchTasks();
  }

  render() {
    return (
      <div className="App">
        <Grid container={true} justify="center" spacing={24}>
          <List component="nav">
            <ActivityList />
            <Footer />
          </List>
        </Grid>
      </div>
    )
  }
}
