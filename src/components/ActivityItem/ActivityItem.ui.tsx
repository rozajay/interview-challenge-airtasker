import * as React from 'react'
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import { DataStruct, ProfileStruct, TaskStruct } from 'src/types/index';

export interface Props {
  data: DataStruct;
  profiles: ProfileStruct[];
  tasks: TaskStruct[];
  NoDisplaySlug?: () => void,
  ChangeSlug: (slugPath: string) => any
}

export class ActivityItem extends React.Component<Props> {

  render() {
    const { data, profiles, tasks, NoDisplaySlug, ChangeSlug } = this.props
    return (
      <ListItem button={true}>
        <span>
          {
            templateMatch(data).map((item, i) => {
              if (item.includes("PROFILES:")) {
                const nameID = parseInt(item.split(/profiles:/gi)[1], 10)
                const ABR_NAME = findNameByID(profiles, nameID)
                return <Link
                  key={i}
                  onMouseEnter={() => { ChangeSlug(findNamePathById(profiles, nameID)) }}
                  onMouseLeave={NoDisplaySlug}
                >
                  {ABR_NAME}
                </Link>
              } else if (item.includes("TASK:")) {
                const taskID = parseInt(item.split(/task:/gi)[1], 10)
                const TASK_NAME = findTaskById(tasks, taskID)
                return <Link
                  key={i}
                  onMouseEnter={() => { ChangeSlug(findTaskPathById(tasks, taskID)) }}
                  onMouseLeave={NoDisplaySlug}
                >
                  {TASK_NAME}
                </Link>
              } else {
                return item.toString()
              }
            })}
        </span>
      </ListItem>
    )
  }
}

function templateMatch(data: DataStruct): string[] {
  const template = data.template.toUpperCase()
  const filtered = template.split(/{|}/gi).filter(item => item !== "")
  return filtered
}

function findNameByID(profiles: ProfileStruct[], idNum: number): string {
  if (idNum == null) {
    return ('')
  } else {
    if (profiles.filter(profile => profile.id === idNum).length === 0) {
      return ('user not found')
    }
    else {
      return (profiles.filter(profile => profile.id === idNum)[0].abbreviated_name)
    }
  }
}

function findTaskById(tasks: TaskStruct[], idNum: number): string {
  if (idNum == null) {
    return ('')
  } else {
    if (tasks.filter(task => task.id === idNum).length === 0) {
      return ('task not found')
    }
    else {
      return (tasks.filter(task => task.id === idNum)[0].name)
    }

  }
}

function findTaskPathById(tasks: TaskStruct[], idNum: number): string {
  if (idNum == null) {
    return ('')
  } else {
    if (tasks.filter(task => task.id === idNum).length === 0) {
      return ('task path not found')
    }
    else {
      return ('/tasks/' + tasks.filter(task => task.id === idNum)[0].slug)
    }

  }
}

function findNamePathById(profiles: ProfileStruct[], idNum: number): string {
  if (idNum == null) {
    return ('')
  } else {
    if (profiles.filter(profile => profile.id === idNum).length === 0) {
      return ('profile path not found')
    }
    else {
      return ('/users/' + profiles.filter(profile => profile.id === idNum)[0].slug)
    }

  }
}