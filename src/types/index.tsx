export interface DataStruct {
  template: string;
  task_id?: number;
  profile_ids: number[];
}

export interface ProfileStruct {
  abbreviated_name: string;
  id: number;
  slug: string;
}

export interface TaskStruct {
  name: string,
  id: number,
  slug: string
}