module.exports = {
  // preset: 'ts-jest',
  testEnvironment: 'jsdom',
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(ts|tsx)?$",
  testPathIgnorePatterns: ["/node_modules/"],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json"],
  collectCoverage: true,
  moduleNameMapper: {
      "\\.(css|less|scss|sass)$": "identity-obj-proxy",
      "\\.(json)$": "identity-obj-proxy"
    },
  collectCoverageFrom: [
    "src/**/*.ts",   "src/**/*.tsx"
  ],
  modulePaths: [ "<rootDir>" ],
  setupFiles: [
     "<rootDir>/__tests__/browserMock.js",
     "<rootDir>/__tests__/test-shim.js",
     "<rootDir>/__tests__/test-setup.js"],
  coverageReporters: [
    "json",
    "text",
    "lcov",
    "clover"
  ],
  globals: {
    "window": true,
    "DEVLOPMENT_MODE": false
  },
  automock: false
};
